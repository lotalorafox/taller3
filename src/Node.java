//node implementation
public class Node {
    //next node
    Node next;
    //data to save, for convinience use int, but can be change to object
    int num;
    //constructor
     Node(int val) {
        num = val;
        next = null;
    }
}
