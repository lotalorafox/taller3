//linked list implementation
public class LinkedList {
    //head of the list
    Node head;
    //constructor recive the first value
    public LinkedList(int val) {
        head = new Node(val);
    }
    //insert the value on the end of the list
    public void append(int val) {
        Node tmpNode = head;
        while (tmpNode.next != null) {
            tmpNode = tmpNode.next;
        }
        tmpNode.next = new Node(val);
    }
    //get the element by the index
    public int get(int index){
        Node tmpNode = head;
        for (int i=0;i<index;i++){
            tmpNode = tmpNode.next;
        }
        return tmpNode.num;
    }
    //insert a vaue in a position in increasing order
    public void insert(int val) {
        Node currentNode = head;
        Node nextNode = head.next;

        if (currentNode.num > val) {
            Node tmpNode = head;
            head = new Node(val);
            head.next = tmpNode;
            return;
        }

        if (nextNode != null && nextNode.num > val) {
            currentNode.next = new Node(val);
            currentNode.next.next = nextNode;
            return;
        }

        while (nextNode != null && nextNode.num < val) {
            currentNode = nextNode;
            nextNode = nextNode.next;
        }

        currentNode.next = new Node(val);
        currentNode.next.next = nextNode;
    }
    //delete a value, if exist more than one delete the fist
    public void delete(int val) {
        Node prevNode = null;
        Node currNode = head;

        if (head.num == val) {
            head = head.next;
            return;
        }

        while (currNode != null && currNode.num != val) {
            prevNode = currNode;
            currNode = currNode.next;
        }

        if (currNode == null) {
            System.out.println("A node with that value does not exist.");
        }
        else {
            prevNode.next = currNode.next;
        }

    }
    //insert a value in a correct position
    public void insert(int index, int val){
        Node n = head;
        Node nue = new Node(val);
        for (int i =0;i<index;i++){
            n = n.next;
        }
        nue.next = n.next;
        n.next = nue;
    }
    //remove a element in a index position
    public void remove(int index){
        Node n = head;
        for (int i =0;i<index;i++){
            n = n.next;
        }
        n.next = n.next.next;
    }
    //print the linked list
    public void print(String [] a) {
        Node tmpNode = head.next;
        while (tmpNode != null) {
            System.out.print(a[tmpNode.num] + " -> ");
            tmpNode = tmpNode.next;
        }
        System.out.print("null");
        System.out.println();
    }
}
