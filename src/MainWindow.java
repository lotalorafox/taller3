import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {
    public MainWindow(){
        add(new MainPanel());
    }
    public static void main(String[] args){
        MainWindow mw = new MainWindow();
        mw.setTitle("Resistor calculator");
        mw.setSize(1200,550);
        mw.setLocationRelativeTo(null);
        mw.setResizable(false);
        mw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mw.setVisible(true);
    }
}
