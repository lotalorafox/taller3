import com.sun.xml.internal.messaging.saaj.soap.JpegDataContentHandler;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.math.*;
import java.util.HashMap;

public class MainPanel extends JPanel {
    Rectangle[] recbands;
    public MainPanel(){
        this.addselectors();

    }
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        BufferedImage img = null;
        int x,y;
        x =150;
        y=150;
        try {
            img = ImageIO.read(new File("image/resistor.png"));
        } catch (IOException e) {
            System.out.println("don't exist");
        }
        g.drawImage(img,x,y,null);
        //g.drawRect(x,y,img.getWidth(),img.getHeight());


    }

    private Image loadImage(String imageName){
        ImageIcon ii = new ImageIcon(imageName);
        return ii.getImage();
    }
    protected void addselectors(){
        //choise of the bands
        Label s = new Label("Select the numbers of bands");
        TextField tx = new TextField("Insert the value of resistor:");
        Button txbutton = new Button("View the colors");
        Choice sbands = new Choice();
        sbands.add("4 bands");
        sbands.add("5 bands");
        sbands.add("6 bands");
        Button bbands = new Button("Send");
        this.add(s);
        this.add(sbands);
        this.add(bbands);
        this.add(tx);
        this.add(txbutton);
        Component[] com = new Component[5];
        com[0] = s;
        com[1] = sbands;
        com[2] = bbands;
        com[3] = tx;
        com[4] = txbutton;
        bbands.addActionListener(new bands(sbands,this,com));
        txbutton.addActionListener(new txlisten(this,com,tx));
    }
    public class bands implements ActionListener {
        String[] colors = {"Black","Brown","Red","Orange","Yellow","Green","Blue","Purple","Grey","White","Gold","Silver"};
        Choice howmany;
        JPanel a;
        Choice[] bans;
        Component[] comp;
        int x,y,b;
        boolean view = false;
        public bands(Choice h, JPanel as, Component[] c){
            System.out.println(h);
            this.howmany =h;
            this.comp = c;
            this.a = as;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            String h = this.howmany.getSelectedItem();
            Button addbands = new Button("set colors");

            x=150;
            y=100;
            if (view){
                a.removeAll();
                Graphics g = a.getGraphics();
                g.clearRect(0,0,1200,550);
                BufferedImage img = null;
                int x,y;
                x =150;
                y=150;
                try {
                    img = ImageIO.read(new File("image/resistor.png"));
                } catch (IOException p) {
                    System.out.println("don't exist");
                }
                g.drawImage(img,x,y,null);
                for (int i=0;i<comp.length;i++){
                    a.add(comp[i]);
                }
            }
            System.out.println(h);
            if(h.equals("4 bands")){
                b=4;
            }else if(h.equals("5 bands")){
                b=5;
            }else{
                b=6;
            }
            bans = new Choice[b];
            for (int i=0;i<b;i++){
                bans[i] = new Choice();
                for (int j=0;j<colors.length;j++){
                    bans[i].add(colors[j]);
                }
                bans[i].setBounds(x,y,70,10);
                x +=80;
                a.add(bans[i]);
            }
            this.view = true;
            addbands.setBounds(x,y,70,20);
            addbands.addActionListener(new drawlines(bans,a,b));
            a.add(addbands);
        }
    }
    public class drawlines implements ActionListener{
        Choice[] ban;
        String[] col;
        int mi;
        LinkedList bandsfinal;
        JPanel a;
        Rectangle[] recbands;
        public drawlines(Choice[] c,JPanel w,int m){
            this.mi = m;
            this.ban = c;
            this.a = w;
            col = new String[c.length];
            System.out.println("Selected: " + Arrays.toString(ban));
            int x =150;
            int y=150;
            recbands = new Rectangle[6];
            recbands[0] = new Rectangle(x+155,y+0,20,235);
            recbands[1] = new Rectangle(x+260,y+37,20,170);
            recbands[2] = new Rectangle(x+400,y+37,20,170);
            recbands[3] = new Rectangle(x+480,y+37,20,170);
            recbands[4] = new Rectangle(x+520,y+37,20,170);
            recbands[5] = new Rectangle(x+660,y+5,20,235);
        }
        public int getindexcolor(String a){
            String[] colors = {"Black","Brown","Red","Orange","Yellow","Green","Blue","Purple","Grey","White","Gold","Silver"};
            for (int j=0;j<colors.length;j++){
                if(a.equals(colors[j])){
                    return j;
                }
            }
            return 0;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Selected: " + Arrays.toString(ban));
            bandsfinal = new LinkedList(0);
            for (int i=0;i<ban.length;i++){
                bandsfinal.append(this.getindexcolor(this.ban[i].getSelectedItem()));
                col[i] = ban[i].getSelectedItem();
            }
            Graphics g =a.getGraphics();
            String[] colors = {"Black","Brown","Red","Orange","Yellow","Green","Blue","Purple","Grey","White","Gold","Silver"};
            String[]  num = {"0","1","2","3","4","5","6","7","8","9","10","11"};
            Color[] color= {Color.decode("#000000"),Color.decode("#451703"),Color.decode("#FF0000"),Color.decode("#FF4500"),Color.decode("#FFFB00"),Color.decode("#08821B"),Color.decode("#000682"),Color.decode("#6C0682"),Color.decode("#787678"),Color.decode("#FEFCFD"),Color.decode("#E8AE13"),Color.decode("#AAAABA")};
            String[] laten = {"0","1","2","0","0","0.5","0.25","0.1","0.05","0","5","10"};
            int can = ban.length;
            int j =5;
            System.out.println(Arrays.toString(col));
            bandsfinal.print(colors);
            bandsfinal.print(num);
            for(int i=(ban.length-1);i>=0;i--){
                System.out.print(bandsfinal.get(i+1) + " - " + this.getindexcolor(col[i]) + " | ");
                g.setColor(color[bandsfinal.get(i+1)]);
                g.drawRect(recbands[j].x,recbands[j].y,recbands[j].width,recbands[j].height);
                g.fillRect(recbands[j].x,recbands[j].y,recbands[j].width,recbands[j].height);
                j--;
            }
            //calculate the value
            int total=0;
            String con ="";
            for (int o=0;o<(ban.length-2);o++){
                con = con + Integer.toString(bandsfinal.get(o+1));
            }
            System.out.println("CON:" + con);
            for (int i=0;i<bandsfinal.get(ban.length-1);i++){
                con = con + "0";
            }
            //total = Integer.parseInt(con);
            //total = (int)(total * (Math.pow(10,(double)(bandsfinal.get(ban.length-1)))));
            con = con + " Ω " + " ± " + laten[bandsfinal.get(ban.length)] + "% ";
            System.out.println("CON " + con);
            g.setColor(Color.BLACK);
            g.setFont(new Font("TimesRoman",Font.PLAIN,50));
            g.drawString(con,400,400);
        }

    }
    public class txlisten implements ActionListener{
        Component[] com;
        JPanel p;
        TextField operation;
        String number;
        Rectangle[] recbands;
        public  txlisten(JPanel w,Component[] h,TextField op){
            this.p=w;
            this.com =h;
            this.operation =op;
            this.number = op.getText();
            int x =150;
            int y=150;
            recbands = new Rectangle[6];
            recbands[0] = new Rectangle(x+155,y+0,20,235);
            recbands[1] = new Rectangle(x+260,y+37,20,170);
            recbands[2] = new Rectangle(x+400,y+37,20,170);
            recbands[3] = new Rectangle(x+480,y+37,20,170);
            recbands[4] = new Rectangle(x+520,y+37,20,170);
            recbands[5] = new Rectangle(x+660,y+5,20,235);
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            number = operation.getText();
            String[] colors = {"Black","Brown","Red","Orange","Yellow","Green","Blue","Purple","Grey","White","Gold","Silver","none"};
            String[]  nume = {"0","1","2","3","4","5","6","7","8","9","10","11","12"};
            Color[] color= {Color.decode("#000000"),Color.decode("#451703"),Color.decode("#FF0000"),Color.decode("#FF4500"),Color.decode("#FFFB00"),Color.decode("#08821B"),Color.decode("#000682"),Color.decode("#6C0682"),Color.decode("#787678"),Color.decode("#FEFCFD"),Color.decode("#E8AE13"),Color.decode("#AAAABA"),new Color(1f,0f,0f,.5f )};
            String[] laten = {"0","1","2","0","0","0.5","0.25","0.1","0.05","0","5","10"};
            LinkedList bands = new LinkedList(12);
            System.out.println(number);
            int num = Integer.parseInt(number);
            int ban =0;
            double a = (double) num;
            int po = num;
            int tem=0;
            while(po>10){
                a = a/10;
                po = po/10;
                tem++;
            }
            tem--;
            ban++;
            a=a/10;
            int nonzero =0;
            System.out.println(a);
            for (int h=0;h<number.length();h++){
                if(number.substring(h,h+1).equals("0")){
                    if(tem >1) {
                        bands.append(0);
                        ban++;
                    }
                    //tem--;
                    break;
                }else {
                    int ntem = Integer.parseInt(number.substring(h,h+1));
                    bands.append(ntem);
                    nonzero++;
                    ban++;
                }
            }
            nonzero = tem -nonzero;
            System.out.println(tem);
            if(nonzero>0)
                tem = tem-nonzero;
            System.out.println("nonzero" + nonzero);
//            while(true){
//                a = a*10;
//                int tema = (int)a;
//                if(tema == 0){
//                    bands.append(0);
//                    ban++;
//                    break;
//                }
//                bands.append(tema);
//                ban++;
//                a = a-tema;
//            }
            bands.append(tem);
            bands.print(colors);
            bands.print(nume);
            Graphics g = p.getGraphics();
            int j =4;
            System.out.println("ban: " + ban);
            for(int i=(ban);i>=1;i--){
                //System.out.print(bands.get(i+1) + " - " + this.getindexcolor(col[i]) + " | ");
                g.setColor(color[bands.get(i)]);
                g.drawRect(recbands[j].x,recbands[j].y,recbands[j].width,recbands[j].height);
                g.fillRect(recbands[j].x,recbands[j].y,recbands[j].width,recbands[j].height);
                j--;
            }
        }
    }
}